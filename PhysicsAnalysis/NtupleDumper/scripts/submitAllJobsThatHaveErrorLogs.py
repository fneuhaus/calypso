#!/usr/bin/env python

import glob
import os
import sys
import pandas as pd

print("NtupleDumping all condor jobs that produced non-empty error logs")

table_runs = pd.read_html('http://aagaard.web.cern.ch/aagaard/FASERruns.html') # load in run tables from website
df = table_runs[0] # table_runs is a list of all tables on the website, we only want the first table

# make a liist of all runs and batch job numbers that failed and thus have error logs that are not empty
run_list = []
allErrorLogs_list = glob.glob('/afs/cern.ch/user/d/dfellers/faser/ntuple-dumper/run/logs/*/*err')
for error_log in allErrorLogs_list:
    if os.path.getsize(error_log) != 0:
        print('Error Log is not empty: ', error_log)
        run_num = int(error_log.split('/')[-2].split('-')[-1])
        bath_num = int(error_log.split('.')[-2])
        run_list.append([run_num,bath_num])

print("list to be re-submitted:", run_list)

ptag="p0008"
filesPerJob=100

for i,run_info in enumerate(run_list):
    runno = run_info[0]
    batch_number = run_info[1]

    runconfig=str(df.at[df.loc[df['Run'] == runno].index[0],'Configuration'].replace(' ','_')) # get config from website run log telling if run is High_gain or Low_gain calo 

    print("%i of %i runs processed. Currently processing run %i-%i (%s)"%(i,len(run_list),runno,batch_number,runconfig))

    batchFile=f"batch/Run-{runno:06d}-{batch_number}.sub"
    fh=open(batchFile,"w")
    pwd=os.getenv("PWD")
    fh.write(f"""
    executable              = {pwd}/analyzeRun.sh
    arguments               = {runno} {batch_number} {filesPerJob} {runconfig}
    output                  = {pwd}/logs/Run-{runno:06d}/batch.{batch_number}.out
    error                   = {pwd}/logs/Run-{runno:06d}/batch.{batch_number}.err
    log                     = {pwd}/logs/Run-{runno:06d}/batch.log
    requirements            = (Arch == "X86_64" && OpSysAndVer =?= "CentOS7")
    getenv                  = False
    transfer_output_files   = ""
    +JobFlavour             = "workday"
    queue 1
    """)
    fh.close()
    os.system(f"echo condor_submit {batchFile}")
    os.system(f"condor_submit {batchFile}")
