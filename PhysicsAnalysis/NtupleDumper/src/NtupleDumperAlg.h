#ifndef NTUPLEDUMPER_NTUPLEDUMPERALG_H
#define NTUPLEDUMPER_NTUPLEDUMPERALG_H

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "AthenaBaseComps/AthHistogramming.h"
#include "TrkTrack/TrackCollection.h"
#include "xAODFaserTrigger/FaserTriggerData.h"
#include "xAODFaserWaveform/WaveformHitContainer.h"
#include "xAODFaserWaveform/WaveformHit.h"
#include "xAODFaserWaveform/WaveformClock.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TrackerPrepRawData/FaserSCT_ClusterContainer.h"
#include "TrackerSpacePoint/FaserSCT_SpacePointContainer.h"
#include "TrackerSimData/TrackerSimDataCollection.h"
#include "FaserActsGeometryInterfaces/IFaserActsExtrapolationTool.h"
#include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"
#include "FaserActsKalmanFilter/IFiducialParticleTool.h"
#include "FaserActsKalmanFilter/ITrackTruthMatchingTool.h"
#include "TrackerSimEvent/FaserSiHitCollection.h"

#include <vector>

class TTree;
class TH1;
class FaserSCT_ID;
class VetoNuID;
class VetoID;
class TriggerID;
class PreshowerID;
class EcalID;
namespace  TrackerDD
{
    class SCT_DetectorManager;
}

class NtupleDumperAlg : public AthReentrantAlgorithm, AthHistogramming {
public:
  NtupleDumperAlg(const std::string &name, ISvcLocator *pSvcLocator);
  virtual ~NtupleDumperAlg() = default;
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext &ctx) const override;
  virtual StatusCode finalize() override;
  const ServiceHandle <ITHistSvc> &histSvc() const;

private:

  bool waveformHitOK(const xAOD::WaveformHit* hit) const;
  void clearTree() const;
  void setNaN() const;
  void addBranch(const std::string &name,float* var);
  void addBranch(const std::string &name,unsigned int* var);
  void addWaveBranches(const std::string &name, int nchannels, int first);
  void FillWaveBranches(const xAOD::WaveformHitContainer &wave) const;

  ServiceHandle <ITHistSvc> m_histSvc;

  SG::ReadHandleKey<xAOD::TruthEventContainer> m_truthEventContainer { this, "EventContainer", "TruthEvents", "Truth event container name." };
  SG::ReadHandleKey<xAOD::TruthParticleContainer> m_truthParticleContainer { this, "ParticleContainer", "TruthParticles", "Truth particle container name." };
  SG::ReadHandleKey<TrackerSimDataCollection> m_simDataCollection {this, "TrackerSimDataCollection", "SCT_SDO_Map"};

  SG::ReadHandleKey<TrackCollection> m_trackCollection { this, "TrackCollection", "CKFTrackCollection", "Input track collection name" };
  SG::ReadHandleKey<TrackCollection> m_trackSegmentCollection {this, "TrackSegmentCollection", "SegmentFit", "Input track segment collection name"};
  SG::ReadHandleKey<xAOD::WaveformHitContainer> m_vetoNuContainer { this, "VetoNuContainer", "VetoNuWaveformHits", "VetoNu hit container name" };
  SG::ReadHandleKey<xAOD::WaveformHitContainer> m_vetoContainer { this, "VetoContainer", "VetoWaveformHits", "Veto hit container name" };
  SG::ReadHandleKey<xAOD::WaveformHitContainer> m_triggerContainer { this, "TriggerContainer", "TriggerWaveformHits", "Trigger hit container name" };
  SG::ReadHandleKey<xAOD::WaveformHitContainer> m_preshowerContainer { this, "PreshowerContainer", "PreshowerWaveformHits", "Preshower hit container name" };
  SG::ReadHandleKey<xAOD::WaveformHitContainer> m_ecalContainer { this, "EcalContainer", "CaloWaveformHits", "Ecal hit container name" };
  SG::ReadHandleKey<Tracker::FaserSCT_ClusterContainer> m_clusterContainer { this, "ClusterContainer", "SCT_ClusterContainer", "Tracker cluster container name" };
  SG::ReadHandleKey<FaserSCT_SpacePointContainer> m_spacePointContainerKey { this, "SpacePoints", "SCT_SpacePointContainer", "space point container"};
  SG::ReadHandleKey<FaserSiHitCollection> m_siHitCollectionKey{this, "FaserSiHitCollection", "SCT_Hits"};

  SG::ReadHandleKey<xAOD::FaserTriggerData> m_FaserTriggerData     { this, "FaserTriggerDataKey", "FaserTriggerData", "ReadHandleKey for xAOD::FaserTriggerData"};
  SG::ReadHandleKey<xAOD::WaveformClock> m_ClockWaveformContainer     { this, "WaveformClockKey", "WaveformClock", "ReadHandleKey for ClockWaveforms Container"};
  ToolHandle<IFaserActsExtrapolationTool> m_extrapolationTool { this, "ExtrapolationTool", "FaserActsExtrapolationTool" };  
  ToolHandle<IFaserActsTrackingGeometryTool> m_trackingGeometryTool {this, "TrackingGeometryTool", "FaserActsTrackingGeometryTool"};
  ToolHandle<ITrackTruthMatchingTool> m_trackTruthMatchingTool {this, "TrackTruthMatchingTool", "TrackTruthMatchingTool"};
  ToolHandle<IFiducialParticleTool> m_fiducialParticleTool {this, "FiducialParticleTool", "FiducialParticleTool"};

  const TrackerDD::SCT_DetectorManager* m_detMgr {nullptr};

  const FaserSCT_ID* m_sctHelper;
  const VetoNuID*    m_vetoNuHelper;
  const VetoID*      m_vetoHelper;
  const TriggerID*   m_triggerHelper;
  const PreshowerID* m_preshowerHelper;
  const EcalID*      m_ecalHelper;

  StringProperty  m_CaloConfig        { this, "CaloConfig", "Low_gain", "Configuration found at http://aagaard.web.cern.ch/aagaard/FASERruns.html (spaces replaced with '_')" };
  BooleanProperty m_doBlinding        { this, "DoBlinding", true, "Blinding will not output events with Calo signal > 10 GeV e-" };
  BooleanProperty m_useFlukaWeights   { this, "UseFlukaWeights", false, "Flag to weight events according to value stored in HepMC::GenEvent" };
  BooleanProperty m_useGenieWeights   { this, "UseGenieWeights", false, "Flag to weight events according to Genie luminosity" };
  IntegerProperty m_flukaCollisions   { this, "FlukaCollisions", 137130000, "Number of proton-proton collisions in FLUKA sample." };
  DoubleProperty  m_flukaCrossSection { this, "FlukaCrossSection", 80.0, "Fluka p-p inelastic cross-section in millibarns." };
  DoubleProperty  m_genieLuminosity   { this, "GenieLuminosity", 150.0, "Genie luminosity in inverse fb." };
  DoubleProperty  m_minMomentum       { this, "MinMomentum", 50000.0, "Write out all truth particles with a momentum larger MinMomentum"};

  double m_baseEventCrossSection {1.0};
  const double kfemtoBarnsPerMilliBarn {1.0e12};

  mutable TTree* m_tree;

  mutable TH1* m_HistRandomCharge[15];

  mutable unsigned int m_run_number;
  mutable unsigned int m_event_number;
  mutable unsigned int m_event_time;
  mutable unsigned int m_bcid;

  mutable unsigned int m_tbp;
  mutable unsigned int m_tap;
  mutable unsigned int m_inputBits;
  mutable unsigned int m_inputBitsNext;

  mutable float m_wave_localtime[15];
  mutable float m_wave_peak[15];
  mutable float m_wave_width[15];
  mutable float m_wave_charge[15];

  mutable float m_wave_raw_peak[15];
  mutable float m_wave_raw_charge[15];
  mutable float m_wave_baseline_mean[15];
  mutable float m_wave_baseline_rms[15];
  mutable unsigned int m_wave_status[15];
  
  mutable float m_calo_total;
  mutable float m_calo_rawtotal;

  mutable float m_Calo0_Edep;
  mutable float m_Calo1_Edep;
  mutable float m_Calo2_Edep;
  mutable float m_Calo3_Edep;
  mutable float m_Calo_Total_Edep;
  mutable float m_Preshower12_Edep;
  mutable float m_Preshower13_Edep;

  mutable float m_MIP_sim_Edep_calo;
  mutable float m_MIP_sim_Edep_preshower;

  mutable float m_clock_phase;

  mutable unsigned int m_station0Clusters;
  mutable unsigned int m_station1Clusters;
  mutable unsigned int m_station2Clusters;
  mutable unsigned int m_station3Clusters;

  mutable unsigned int m_nspacepoints;
  mutable std::vector<double> m_spacepointX;
  mutable std::vector<double> m_spacepointY;
  mutable std::vector<double> m_spacepointZ;

  mutable unsigned int m_ntracksegs;
  mutable std::vector<double> m_trackseg_Chi2;
  mutable std::vector<double> m_trackseg_DoF;
  mutable std::vector<double> m_trackseg_x;
  mutable std::vector<double> m_trackseg_y;
  mutable std::vector<double> m_trackseg_z;
  mutable std::vector<double> m_trackseg_px;
  mutable std::vector<double> m_trackseg_py;
  mutable std::vector<double> m_trackseg_pz;

  mutable int    m_longTracks;
  mutable std::vector<double> m_Chi2;
  mutable std::vector<double> m_DoF;
  mutable std::vector<double> m_xup;
  mutable std::vector<double> m_yup;
  mutable std::vector<double> m_zup;
  mutable std::vector<double> m_pxup;
  mutable std::vector<double> m_pyup;
  mutable std::vector<double> m_pzup;
  mutable std::vector<double> m_pup;
  mutable std::vector<double> m_xdown;
  mutable std::vector<double> m_ydown;
  mutable std::vector<double> m_zdown;
  mutable std::vector<double> m_pxdown;
  mutable std::vector<double> m_pydown;
  mutable std::vector<double> m_pzdown;
  mutable std::vector<double> m_pdown;
  mutable std::vector<int> m_charge;
  mutable std::vector<unsigned int> m_nLayers;
  mutable std::vector<unsigned int> m_nHit0;
  mutable std::vector<unsigned int> m_nHit1;
  mutable std::vector<unsigned int> m_nHit2;
  mutable std::vector<unsigned int> m_nHit3;
  mutable std::vector<double> m_xVetoNu;
  mutable std::vector<double> m_yVetoNu;
  mutable std::vector<double> m_thetaxVetoNu;
  mutable std::vector<double> m_thetayVetoNu;
  mutable std::vector<double> m_xVetoStation1;
  mutable std::vector<double> m_yVetoStation1;
  mutable std::vector<double> m_thetaxVetoStation1;
  mutable std::vector<double> m_thetayVetoStation1;
  mutable std::vector<double> m_xVetoStation2;
  mutable std::vector<double> m_yVetoStation2;
  mutable std::vector<double> m_thetaxVetoStation2;
  mutable std::vector<double> m_thetayVetoStation2;
  mutable std::vector<double> m_xTrig;
  mutable std::vector<double> m_yTrig;
  mutable std::vector<double> m_thetaxTrig;
  mutable std::vector<double> m_thetayTrig;
  mutable std::vector<double> m_xPreshower1;
  mutable std::vector<double> m_yPreshower1;
  mutable std::vector<double> m_thetaxPreshower1;
  mutable std::vector<double> m_thetayPreshower1;
  mutable std::vector<double> m_xPreshower2;
  mutable std::vector<double> m_yPreshower2;
  mutable std::vector<double> m_thetaxPreshower2;
  mutable std::vector<double> m_thetayPreshower2;
  mutable std::vector<double> m_xCalo;
  mutable std::vector<double> m_yCalo;
  mutable std::vector<double> m_thetaxCalo;
  mutable std::vector<double> m_thetayCalo;

  mutable std::vector<int> m_t_pdg; // pdg code of the truth matched particle
  mutable std::vector<int> m_t_barcode; // barcode of the truth matched particle
  mutable std::vector<double> m_t_truthHitRatio; // ratio of hits on track matched to the truth particle over all hits on track
  mutable std::vector<double> m_t_prodVtx_x; // x component of the production vertex in mm
  mutable std::vector<double> m_t_prodVtx_y; // y component of the production vertex in mm
  mutable std::vector<double> m_t_prodVtx_z; // z component of the production vertex in mm
  mutable std::vector<double> m_t_decayVtx_x; // x component of the decay vertex in mm
  mutable std::vector<double> m_t_decayVtx_y; // y component of the decay vertex in mm
  mutable std::vector<double> m_t_decayVtx_z; // z component of the decay vertex in mm
  mutable std::vector<double> m_t_px; // truth momentum px in MeV
  mutable std::vector<double> m_t_py;  // truth momentum py in MeV
  mutable std::vector<double> m_t_pz;  // truth momentum pz in MeV
  mutable std::vector<double> m_t_theta; // angle of truth particle with respsect to the beam axis in rad, theta = arctan(sqrt(px * px + py * py) / pz)
  mutable std::vector<double> m_t_phi; // polar angle of truth particle in rad, phi = arctan(py / px)
  mutable std::vector<double> m_t_p; // truth momentum p in MeV
  mutable std::vector<double> m_t_pT; // transverse truth momentum pT in MeV
  mutable std::vector<double> m_t_eta; // eta of truth particle
  mutable std::array<std::vector<double>, 4> m_t_st_x; // vector of the x components of the simulated hits of the truth particle for each station
  mutable std::array<std::vector<double>, 4> m_t_st_y; // vector of the y components of the simulated hits of the truth particle for each station
  mutable std::array<std::vector<double>, 4> m_t_st_z; // vector of the z components of the simulated hits of the truth particle for each station
  mutable std::vector<bool> m_isFiducial; // track is fiducial if there are simulated hits for stations 1 - 3 and the distance from the center is smaller than 100 mm

  mutable std::vector<int> m_truthParticleBarcode; // vector of barcodes of all truth particles with a momentum larger 50 GeV
  mutable std::vector<int> m_truthParticleMatchedTracks; // vector of number of tracks to which a truth particle is matched to
  mutable std::vector<bool> m_truthParticleIsFiducial; // vector of boolean showing whether a truth particle is fiducial

  mutable double m_truthLeptonMomentum;
  mutable int    m_truthBarcode;
  mutable int    m_truthPdg;
  mutable double m_crossSection;

};

inline const ServiceHandle <ITHistSvc> &NtupleDumperAlg::histSvc() const {
  return m_histSvc;
}

#endif  // NTUPLEDUMPER_NTUPLEDUMPERALG_H
