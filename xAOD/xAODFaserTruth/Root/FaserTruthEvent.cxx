/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: FaserTruthEvent.cxx 761798 2016-07-14 08:15:01Z krasznaa $

// System include(s):
#include <cmath>

// EDM include(s):
#include "xAODCore/AuxStoreAccessorMacros.h"

// Local include(s):
#include "xAODFaserTruth/FaserTruthEvent.h"
#include "FaserTruthAccessors.h"

namespace xAOD {


   FaserTruthEvent::FaserTruthEvent()
      : SG::AuxElement() {

   }
   
   /////////////////////////////////////////////////////////////////////////////
   // Implementation of the truth particle accessor functions

   AUXSTORE_OBJECT_SETTER_AND_GETTER( FaserTruthEvent,
                                      FaserTruthEvent::FaserTruthParticleLinks_t,
                                      truthParticleLinks,
                                      setFaserTruthParticleLinks )

   /// Accessor for the truth particles
   static SG::AuxElement::Accessor< FaserTruthEvent::FaserTruthParticleLinks_t >
      truthParticleLinksAcc( "truthParticleLinks" );

   size_t FaserTruthEvent::nFaserTruthParticles() const {

      // If the variable is not available, we don't have any truth particles
      // associated...
      if( ! truthParticleLinksAcc.isAvailable( *this ) ) {
         return 0;
      }

      // Return the size of the vector:
      return truthParticleLinksAcc( *this ).size();
   }

   const FaserTruthEvent::FaserTruthParticleLink_t&
   FaserTruthEvent::truthParticleLink( size_t index ) const {

      return truthParticleLinksAcc( *this ).at( index );
   }

   const FaserTruthParticle* FaserTruthEvent::truthParticle( size_t index ) const {

      // Check if the variable is available:
      if( ! truthParticleLinksAcc.isAvailable( *this ) ) {
         return 0;
      }

      // Check if the link is valid:
      const FaserTruthParticleLinks_t& links = truthParticleLinksAcc( *this );
      if( ! links[ index ].isValid() ) {
         return 0;
      }

      // Return the de-referenced link:
      return *( links[ index ] );
   }

   void
   FaserTruthEvent::addFaserTruthParticleLink( const FaserTruthParticleLink_t& link ) {

      truthParticleLinksAcc( *this ).push_back( link );
      return;
   }

   void FaserTruthEvent::clearFaserTruthParticleLinks() {

      truthParticleLinksAcc( *this ).clear();
      return;
   }

   /////////////////////////////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////
   // Implementation of the truth vertex accessor functions

   AUXSTORE_OBJECT_SETTER_AND_GETTER( FaserTruthEvent,
                                      FaserTruthEvent::FaserTruthVertexLinks_t,
                                      truthVertexLinks,
                                      setFaserTruthVertexLinks )

   /// Accessor for the truth vertices
   static SG::AuxElement::Accessor< FaserTruthEvent::FaserTruthVertexLinks_t >
      truthVertexLinksAcc( "truthVertexLinks" );

   size_t FaserTruthEvent::nTruthVertices() const {

      // If the variable is not available, we don't have any truth particles
      // associated...
      if( ! truthVertexLinksAcc.isAvailable( *this ) ) {
         return 0;
      }

      // Return the size of the vector:
      return truthVertexLinksAcc( *this ).size();
   }

   const FaserTruthEvent::FaserTruthVertexLink_t&
   FaserTruthEvent::truthVertexLink( size_t index ) const {

      return truthVertexLinksAcc( *this ).at(index);
   }

   const FaserTruthVertex* FaserTruthEvent::truthVertex( size_t index ) const {

      // Check if the variable is available:
      if( ! truthVertexLinksAcc.isAvailable( *this ) ) {
         return 0;
      }

      // Check if the link is valid:
      const FaserTruthVertexLinks_t& links = truthVertexLinksAcc( *this );
      if( ! links[ index ].isValid() ) {
         return 0;
      }

      // Return the de-referenced link:
      return *( links[ index ] );
   }

   void FaserTruthEvent::addFaserTruthVertexLink( const FaserTruthVertexLink_t& link ) {

      truthVertexLinksAcc( *this ).push_back( link );
      return;
   }

   void FaserTruthEvent::clearFaserTruthVertexLinks() {

      truthVertexLinksAcc( *this ).clear();
      return;
   }

   
   /////////////////////////////////////////////////////////////////////////////
   // Simple, always-present event properties

   /// @todo Need upgrade to allow string-valued map-like access... or access a
   /// corresponding vector of names
   AUXSTORE_OBJECT_SETTER_AND_GETTER( FaserTruthEvent, std::vector< float >,
                                      weights, setWeights )

   AUXSTORE_PRIMITIVE_SETTER_AND_GETTER( FaserTruthEvent, float, crossSection,
                                         setCrossSection )
   AUXSTORE_PRIMITIVE_SETTER_AND_GETTER( FaserTruthEvent, float,
                                         crossSectionError,
                                         setCrossSectionError )

   void FaserTruthEvent::setCrossSection( float value, float error ) {

      setCrossSection( value );
      setCrossSectionError( error );
      return;
   }

   /////////////////////////////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////
   // Optional PDF info accessors

   bool FaserTruthEvent::pdfInfoParameter( int& value,
                                         PdfParam information ) const {

      // Look for the accessor object:
      auto* acc = pdfInfoAccessorInt( information );
      if( ! acc ) return false;

      // Get the value:
      value = ( *acc )( *this );
      return true;
   }

   bool FaserTruthEvent::pdfInfoParameter( float& value,
                                         PdfParam information ) const {

      // Look for the accessor object:
      auto* acc = pdfInfoAccessorFloat( information );
      if( ! acc ) return false;

      // Get the value:
      value = ( *acc )( *this );
      return true;
   }

   bool FaserTruthEvent::setPdfInfoParameter( int value,
                                            PdfParam information ) {

      // Look for the accessor object:
      auto* acc = pdfInfoAccessorInt( information );
      if( ! acc ) return false;

      // Set the value:
      ( *acc )( *this ) = value;
      return true;
   }

   bool FaserTruthEvent::setPdfInfoParameter( float value,
                                            PdfParam information ) {

      // Look for the accessor object:
      auto* acc = pdfInfoAccessorFloat( information );
      if( ! acc ) return false;

      // Set the value:
      ( *acc )( *this ) = value;
      return true;
   }

   FaserTruthEvent::PdfInfo::PdfInfo()
      : pdgId1( 0 ), pdgId2( 0 ), pdfId1( -1 ), pdfId2( -1 ),
        x1( NAN ), x2( NAN ), Q( NAN ), xf1( NAN ), xf2( NAN ) {

   }

   bool FaserTruthEvent::PdfInfo::valid() const {

      return ( ( pdgId1 != 0 ) && ( pdgId2 != 0 ) &&
               ( pdfId1 >= 0 ) && ( pdfId2 >= 0 ) &&
               ( ! std::isnan( x1 ) ) && ( ! std::isnan( x2 ) ) &&
               ( ! std::isnan( Q ) ) &&
               ( ! std::isnan( xf1 ) ) && ( ! std::isnan( xf2 ) ) );
   }

   FaserTruthEvent::PdfInfo FaserTruthEvent::pdfInfo() const {

      // The result object:
      PdfInfo rtn;

      // Retrieve all of its elements:
      pdfInfoParameter( rtn.pdgId1, PDGID1 );
      pdfInfoParameter( rtn.pdgId2, PDGID2 );
      pdfInfoParameter( rtn.pdfId1, PDFID1 );
      pdfInfoParameter( rtn.pdfId2, PDFID2 );
      pdfInfoParameter( rtn.x1,     X1 );
      pdfInfoParameter( rtn.x2,     X2 );
      pdfInfoParameter( rtn.Q,      Q );
      pdfInfoParameter( rtn.xf1,    XF1 );
      pdfInfoParameter( rtn.xf2,    XF2 );

      return rtn;
   }

   /////////////////////////////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////
   // Implementation for the links to truth particles/vertices

   // Accessor for the signal vertex
   static SG::AuxElement::Accessor< FaserTruthEvent::FaserTruthVertexLink_t >
      signalProcessVertexLinkAcc( "signalProcessVertexLink" );

   const FaserTruthVertex* FaserTruthEvent::signalProcessVertex() const {

      // Check if the link variable is available:
      if( ! signalProcessVertexLinkAcc.isAvailable( *this ) ) {
         return 0;
      }

      // Get the link:
      const FaserTruthVertexLink_t& vertLink = signalProcessVertexLinkAcc( *this );

      // Check if it's valid:
      if( ! vertLink.isValid() ) {
         return 0;
      }

      // Return the de-referenced link:
      return *vertLink;
   }

   AUXSTORE_OBJECT_SETTER_AND_GETTER( FaserTruthEvent,
                                      FaserTruthEvent::FaserTruthVertexLink_t,
                                      signalProcessVertexLink,
                                      setSignalProcessVertexLink )

   // Accessors for the beam particles
   static SG::AuxElement::Accessor< FaserTruthEvent::FaserTruthParticleLink_t >
      primaryParticleLinkLinkAcc( "primaryParticleLink" );

   AUXSTORE_OBJECT_SETTER_AND_GETTER( FaserTruthEvent,
                                      FaserTruthEvent::FaserTruthParticleLink_t,
                                      primaryParticleLink,
                                      setPrimaryParticleLink )

   /////////////////////////////////////////////////////////////////////////////

   FaserType::ObjectType FaserTruthEvent::faserType() const {

      return FaserType::FaserTruthEvent;
   }
   
   Type::ObjectType FaserTruthEvent::type() const {

      return Type::Other;
   }
   
   AUXSTORE_OBJECT_SETTER_AND_GETTER(FaserTruthEvent, std::vector < std::string >, weightNames, setWeightNames )
   AUXSTORE_PRIMITIVE_SETTER_AND_GETTER(FaserTruthEvent, uint32_t, mcChannelNumber, setMcChannelNumber )

   void FaserTruthEvent::toPersistent() {

      if( primaryParticleLinkLinkAcc.isAvailableWritable( *this ) ) {
         primaryParticleLinkLinkAcc( *this ).toPersistent();
      }

      if( signalProcessVertexLinkAcc.isAvailableWritable( *this ) ) {
         signalProcessVertexLinkAcc( *this ).toPersistent();
      }

      // Prepare the truth particle links for writing:
      if( truthParticleLinksAcc.isAvailableWritable( *this ) ) {
         FaserTruthParticleLinks_t::iterator itr =
            truthParticleLinksAcc( *this ).begin();
         FaserTruthParticleLinks_t::iterator end =
            truthParticleLinksAcc( *this ).end();
         for( ; itr != end; ++itr ) {
            itr->toPersistent();
         }
      }

      // Prepare the truth vertex links for writing:
      if( truthVertexLinksAcc.isAvailableWritable( *this ) ) {
         FaserTruthVertexLinks_t::iterator itr =
            truthVertexLinksAcc( *this ).begin();
         FaserTruthVertexLinks_t::iterator end =
            truthVertexLinksAcc( *this ).end();
         for( ; itr != end; ++itr ) {
            itr->toPersistent();
         }
      }
      return;
   }

} // namespace xAOD
