#include "PairVertexAlg.h"

#include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"
#include "GeoPrimitives/CLHEPtoEigenConverter.h"
#include "Acts/Surfaces/PerigeeSurface.hpp"
#include "TrackerIdentifier/FaserSCT_ID.h"
#include "TrackerReadoutGeometry/SCT_DetectorManager.h"
#include "TrackerReadoutGeometry/SiDetectorElement.h"
#include <cmath>


namespace Tracker {

PairVertexAlg::PairVertexAlg(const std::string &name, ISvcLocator *pSvcLocator)
    : AthReentrantAlgorithm(name, pSvcLocator) {}

StatusCode PairVertexAlg::initialize() 
{
  ATH_CHECK(m_mcEventCollectionKey.initialize());
  ATH_CHECK(m_trackCollectionKey.initialize());
  ATH_CHECK(m_extrapolationTool.retrieve());
  ATH_CHECK(detStore()->retrieve(m_idHelper, "FaserSCT_ID"));
  ATH_CHECK(detStore()->retrieve(m_detMgr, "SCT"));
  return StatusCode::SUCCESS;
}

StatusCode PairVertexAlg::execute(const EventContext &ctx) const 
{
    m_numberOfEvents++;
    const Acts::GeometryContext gctx =
        m_extrapolationTool->trackingGeometryTool()->getNominalGeometryContext().context();

    std::vector<double> z_positions {};

    SG::ReadHandle<McEventCollection> mcEvents {m_mcEventCollectionKey, ctx};
    ATH_CHECK(mcEvents.isValid());
    if (mcEvents->size() != 1) 
    {
        ATH_MSG_ERROR("There should be exactly one event in the McEventCollection.");
        return StatusCode::FAILURE;
    }

    SG::ReadHandle<TrackCollection> tracks { m_trackCollectionKey, ctx };
    ATH_CHECK(tracks.isValid());

    const Trk::TrackParameters* positive {nullptr};
    const Trk::TrackParameters* negative {nullptr};

    for (auto trk : *tracks)
    {
        const Trk::TrackParameters* upstream {nullptr};
        if (trk == nullptr)
        {
            ATH_MSG_WARNING("Null pointer from TrackContainer.");
            m_invalidTrackContainerEntries++;
            continue;
        }
        auto parameters = trk->trackParameters();
        if (parameters == nullptr || parameters->empty()) 
        {
            m_numberOfNoParameterTracks++;
            ATH_MSG_WARNING("Track without parameters found.");
            return StatusCode::SUCCESS;
        }
        for (auto state : *parameters)
        {
            if (state == nullptr)
            {
                m_numberOfNullParameters++;
                ATH_MSG_WARNING("Null track parameters returned.");
                continue;
            }
            if (!state->hasSurface())
            {
                m_numberOfParametersWithoutSurface++;
                ATH_MSG_WARNING("Track state without surface found.");
                continue;
            }
            if (!upstream || upstream->associatedSurface().center().z() > state->associatedSurface().center().z())
                upstream = state;
        }
        if (!upstream)
        {
            m_numberOfNoUpstreamTracks++;
            ATH_MSG_WARNING("Unable to find track parameters on any surface");
            continue;
        }
        auto momentum = upstream->momentum();
        double charge = upstream->charge();
        if (charge > 0 || momentum.mag() > positive->momentum().mag())
        {
            positive = upstream;
        }
        else if (charge < 0 || momentum.mag() > negative->momentum().mag())
        {
            negative = upstream;
        }
    }

    if (positive != nullptr) m_numberOfPositiveTracks++;
    if (negative != nullptr) m_numberOfNegativeTracks++;

    if (positive != nullptr && negative != nullptr) m_numberOfOppositeSignPairs++;

    // for (const HepMC::GenParticle* particle : mcEvents->front()->particle_range()) 
    // {
    //     if ((std::abs(particle->pdg_id()) != 13)) continue;
    //     const HepMC::FourVector& vertex = particle->production_vertex()->position();
    //     if (vertex.z() > 0) continue;
    //     const HepMC::FourVector& momentum = particle->momentum();
    //     double phi = momentum.phi();
    //     double theta = momentum.theta();
    //     double charge = particle->pdg_id() > 0 ? -1 : 1;
    //     double abs_momentum = momentum.rho() * m_MeV2GeV;
    //     double qop = charge / abs_momentum;
    //     // The coordinate system of the Acts::PlaneSurface is defined as
    //     // T = Z = normal, U = X x T = -Y, V = T x U = x
    // //    Acts::BoundVector pars;
    // //    pars << -vertex.y(), vertex.x(), phi, theta, qop, vertex.t();
    //     Acts::BoundVector pars = Acts::BoundVector::Zero();
    //     pars[Acts::eBoundLoc0] = -vertex.y();
    //     pars[Acts::eBoundLoc1] = vertex.x();
    //     pars[Acts::eBoundPhi] = phi;
    //     pars[Acts::eBoundTheta] = theta;
    //     pars[Acts::eBoundQOverP] = qop;
    //     pars[Acts::eBoundTime] = vertex.t();

    //     auto startSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
    //         Acts::Vector3(0, 0, vertex.z()), Acts::Vector3(0, 0, 1));
    //     auto targetSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
    //         Acts::Vector3(0, 0, z_mean), Acts::Vector3(0, 0, 1));
    //     Acts::BoundTrackParameters startParameters(
    //         std::move(startSurface), pars, charge);
    //     ATH_MSG_DEBUG("vertex: " << vertex.x() << ", " << vertex.y() << ", " << vertex.z());
    //     ATH_MSG_DEBUG("vertex momentum: " << momentum.x() * m_MeV2GeV << ", " << momentum.y() * m_MeV2GeV << ", " << momentum.z() * m_MeV2GeV);
    //     std::unique_ptr<const Acts::BoundTrackParameters> targetParameters =
    //         m_extrapolationTool->propagate(ctx, startParameters, *targetSurface);
    //     if (targetParameters) 
    //     {
    //         Acts::Vector3 targetPosition = targetParameters->position(gctx);
    //         Acts::Vector3 targetMomentum = targetParameters->momentum();
    //         ATH_MSG_DEBUG("vertex: " << vertex.x() << ", " << vertex.y() << ", " << vertex.z());
    //         ATH_MSG_DEBUG("origin: " << targetPosition.x() << ", " << targetPosition.y() << ", " << targetPosition.z());
    //         ATH_MSG_DEBUG("vertex momentum: " << momentum.x() * m_MeV2GeV << ", " << momentum.y() * m_MeV2GeV << ", " << momentum.z() * m_MeV2GeV);
    //         ATH_MSG_DEBUG("origin momentum: " << targetMomentum.x() << ", " << targetMomentum.y() << ", " << targetMomentum.z());
    //     }
    // }

  return StatusCode::SUCCESS;
}

StatusCode PairVertexAlg::finalize() 
{
    ATH_MSG_INFO("Found " << m_numberOfOppositeSignPairs << " opposite sign pairs in " << m_numberOfEvents << " total events.");
    ATH_MSG_INFO(m_numberOfPositiveTracks << " events had a positive track, and " << m_numberOfNegativeTracks << " had a negative track.");
    ATH_MSG_INFO(m_numberOfNoUpstreamTracks << " tracks could not locate an upstream position.");
    ATH_MSG_INFO(m_numberOfNoParameterTracks << " tracks in track pairs had no track parameters.");
    ATH_MSG_INFO(m_numberOfNullParameters << " track parameters were null and " << m_numberOfParametersWithoutSurface << " had no associated surface.");
    ATH_MSG_INFO(m_invalidTrackContainerEntries << " invalid TrackContainer entries found.");
    return StatusCode::SUCCESS;
}

} // Tracker
