# Copyright (C) 2020-2021 CERN for the benefit of the FASER collaboration

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg

from WaveformConditionsTools.WaveformCableMappingConfig import WaveformCableMappingCfg

# Crystallball parameter dictionary used in simulated digitized wave reconstruction.
# Crystalball function Parameters estimated from Deion's slides uploaded at
# https://indico.cern.ch/event/1099652/contributions/4626975/attachments/2352595/4013927/Faser-Physics-run3933-plots.pdf  (20/01/2022)
# Parameters are per scintillator source, but not per channel.
# Updated aamplitudes (norm) to match testbeam response
# Make everything except VetoNu look like the preshower
dict_CB_param = {}
dict_CB_param["Trigger"]=dict(CB_alpha=-0.424, CB_n=6.14, CB_mean=815, CB_sigma=3.21, CB_norm = 4240) 
dict_CB_param["Timing"] =dict(CB_alpha=-0.424, CB_n=6.14, CB_mean=846, CB_sigma=3.21, CB_norm = 4240) 
dict_CB_param["Veto"]   =dict(CB_alpha=-0.32,  CB_n=9.0,  CB_mean=815, CB_sigma=3.35, CB_norm = 6840) 
dict_CB_param["VetoNu"] =dict(CB_alpha=-0.28,  CB_n=1000, CB_mean=815, CB_sigma=5.00, CB_norm = 7040) 
dict_CB_param["Preshower"]=dict(CB_alpha=-0.32, CB_n=1000, CB_mean=846, CB_sigma=4.0, CB_norm =  400)

dict_baseline_params = {
    "Trigger"   : {"mean" : 15650, "rms" : 3},
    "Timing"    : {"mean" : 15650, "rms" : 3},
    "Veto"      : {"mean" : 15650, "rms" : 3},
    "VetoNu"    : {"mean" : 15650, "rms" : 3},
    "Preshower" : {"mean" : 15650, "rms" : 3},        
    }

# One stop shopping for normal FASER data
def ScintWaveformDigitizationCfg(flags):
    """ Return all algorithms and tools for Waveform digitization """
    acc = ComponentAccumulator()

    if not flags.Input.isMC:
        return acc

    if "TB" in flags.GeoModel.FaserVersion:
        acc.merge(ScintWaveformDigiCfg(flags, "VetoWaveformDigiAlg", "Veto"))
        acc.merge(ScintWaveformDigiCfg(flags, "PreshowerWaveformDigiAlg", "Preshower"))
    else:
        acc.merge(ScintWaveformDigiCfg(flags, "TriggerWaveformDigiAlg", "Trigger"))
        acc.merge(ScintWaveformDigiCfg(flags, "VetoWaveformDigiAlg", "Veto"))
        acc.merge(ScintWaveformDigiCfg(flags, "VetoNuWaveformDigiAlg", "VetoNu"))
        acc.merge(ScintWaveformDigiCfg(flags, "PreshowerWaveformDigiAlg", "Preshower"))

    acc.merge(ScintWaveformDigitizationOutputCfg(flags))
    acc.merge(WaveformCableMappingCfg(flags))
    return acc

# Return configured digitization algorithm from SIM hits
# Specify data source (Veto, Trigger, Preshower)
def ScintWaveformDigiCfg(flags, name="ScintWaveformDigiAlg", source="", **kwargs):

    acc = ComponentAccumulator()

    tool = CompFactory.WaveformDigitisationTool(name=source+"WaveformDigtisationTool", **kwargs)
    
    kwargs.setdefault("ScintHitContainerKey", source+"Hits")
    kwargs.setdefault("WaveformContainerKey", source+"Waveforms")

    digiAlg = CompFactory.ScintWaveformDigiAlg(name, **kwargs)

    if "TB" in flags.GeoModel.FaserVersion and source == "Veto":
        # The testbeam counters were actually VetoNu, so use those parameters 
        source = "VetoNu"

    digiAlg.CB_alpha = dict_CB_param[source]["CB_alpha"]
    digiAlg.CB_n = dict_CB_param[source]["CB_n"]
    digiAlg.CB_mean = dict_CB_param[source]["CB_mean"]
    digiAlg.CB_sigma = dict_CB_param[source]["CB_sigma"]
    digiAlg.CB_norm = dict_CB_param[source]["CB_norm"]    

    digiAlg.base_mean = dict_baseline_params[source]["mean"]
    digiAlg.base_rms = dict_baseline_params[source]["rms"]
     
    kwargs.setdefault("WaveformDigitisationTool", tool)

    acc.addEventAlgo(digiAlg)

    return acc

def ScintWaveformDigitizationOutputCfg(flags, **kwargs):
    """ Return ComponentAccumulator with output for Waveform Digi"""
    acc = ComponentAccumulator()
    ItemList = [
        "RawWaveformContainer#*"
    ]
    acc.merge(OutputStreamCfg(flags, "RDO"))
    ostream = acc.getEventAlgo("OutputStreamRDO")
    # ostream.TakeItemsFromInput = True # Copies all data from input file to output
    # ostream.TakeItemsFromInput = False
    ostream.ItemList += ItemList
    return acc
