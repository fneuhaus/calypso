/*
  Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

/**
 * @file WaveformDigitisationTool.cxx
 * Implementation file for the WaveformDigitisationTool class
 * @ author C. Gwilliam, 2021
 **/

#include "WaveformDigitisationTool.h"

// Constructor
WaveformDigitisationTool::WaveformDigitisationTool(const std::string& type, const std::string& name, const IInterface* parent) :
  base_class(type, name, parent)
{
}

// Initialization
StatusCode
WaveformDigitisationTool::initialize() {
  ATH_MSG_INFO( name() << "::initalize()" );

  m_nsamples = 600;
  m_random = new TRandom3();

  return StatusCode::SUCCESS;
}


std::vector<float>
WaveformDigitisationTool::evaluate_timekernel(TF1* kernel) const {
  
  std::vector<float> timekernel;
  timekernel.reserve(m_nsamples);
  
  for (unsigned int i=0; i<m_nsamples; i++) {
    timekernel.push_back(kernel->Eval(2.*i));  
  }
  
  return timekernel;
}

unsigned int 
WaveformDigitisationTool::generate_baseline(int mean, int rms) const {
  return m_random->Gaus(mean, rms);
}
